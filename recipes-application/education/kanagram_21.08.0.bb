# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kanagram-21.08.0.tar.xz"
SRC_URI[sha256sum] = "3f721216f0ef3800f94d29bc8ce2259ec5596a6ca028934c5a7d05ac73c867aa"
