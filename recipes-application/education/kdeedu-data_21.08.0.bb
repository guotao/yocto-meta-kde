# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kdeedu-data-21.08.0.tar.xz"
SRC_URI[sha256sum] = "8ff0200898569c6c4135d9b66006b504ed7cc9fdd472b7b2cb987c622cc3f3eb"
