# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kpublictransport-21.08.0.tar.xz"
SRC_URI[sha256sum] = "a265c8ae39da47dfe14189da588db20bbe179ebd20abe953f9ef889f40c9dead"
