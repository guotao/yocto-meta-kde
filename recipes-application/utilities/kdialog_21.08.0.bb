# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kdialog-21.08.0.tar.xz"
SRC_URI[sha256sum] = "d4af3cd41b34a4dc3f3ef23238f9791f1470ce5362c2edbf6354e6ce1b0471de"
