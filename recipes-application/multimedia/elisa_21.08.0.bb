# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/elisa-21.08.0.tar.xz"
SRC_URI[sha256sum] = "d57b46c0d1518b50f2875c5eca20006389f64e91560146fda7f0a87852e7f493"
