# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
# use unreleased tag for fixing build with Qt 5.15.3
SRCREV = "c257a40ba95f56b5f3830b923b1c56aa055cf8ea"
PV = "0.19.0+git${SRCPV}"
