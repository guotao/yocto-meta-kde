# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kscreen-5.25.3.tar.xz"
SRC_URI[sha256sum] = "7f534944c81e7ca35b676cff065c64deebe2ec2fe06b31849465254a5ffc375d"

