# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kde-cli-tools-5.25.3.tar.xz"
SRC_URI[sha256sum] = "300c28d36433985152ac88ffb3279af1d1759bc8cd07bead8a92bbfe6c281b55"

