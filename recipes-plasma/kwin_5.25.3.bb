# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kwin-5.25.3.tar.xz"
SRC_URI[sha256sum] = "52927c7b6a2f28c6b8a515b1eb6f7faeacdc0d53321290887ebaffb9ab2fd0ef"

