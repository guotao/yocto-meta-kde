# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kscreenlocker-5.25.3.tar.xz"
SRC_URI[sha256sum] = "8c8f99c49932e9ebba0738f8fa302718485d26403c730096e75b2524471e31ce"

