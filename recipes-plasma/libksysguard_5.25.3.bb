# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/libksysguard-5.25.3.tar.xz"
SRC_URI[sha256sum] = "094503e82a9782e31ed8ed58487cca2fd14da168196fa7b89c3657bc65cf39d7"

