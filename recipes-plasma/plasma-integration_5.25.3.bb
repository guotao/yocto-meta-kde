# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-integration-5.25.3.tar.xz"
SRC_URI[sha256sum] = "53aec648aa30e5de4e4ba32e4a76fc20fd6e984f8afd030529444eb0d6065ff3"

