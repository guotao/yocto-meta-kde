# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-mobile-5.25.3.tar.xz"
SRC_URI[sha256sum] = "e325e1b4977f01a551003d4947a17eca1b1f93bc83a5546903b9d52f1e5eeeb7"

