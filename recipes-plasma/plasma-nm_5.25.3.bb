# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-nm-5.25.3.tar.xz"
SRC_URI[sha256sum] = "a74bd66009712a52aa2b7dfc1c895e5a4e234b7723f84975b8b434e4a5e41f4d"

