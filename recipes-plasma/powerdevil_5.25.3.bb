# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/powerdevil-5.25.3.tar.xz"
SRC_URI[sha256sum] = "02ae3c4eef7d254aac687557f7f092e1a89beb3390ac77eadb784d2133d0dcd1"

