# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kwayland-integration-5.25.3.tar.xz"
SRC_URI[sha256sum] = "4eae00d046542dbcab47568bfd78b1e708837df4a82ae982297b5fdd5fe48534"

