# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-workspace-5.25.3.tar.xz"
SRC_URI[sha256sum] = "0708bf77b0cf63f085dcc7002a1630ba6930c61cdfe43769ff4baa1fd545b3a0"

