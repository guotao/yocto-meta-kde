# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kactivitymanagerd-5.25.3.tar.xz"
SRC_URI[sha256sum] = "a7746ec9dbb0dd19d6fffb74ef3b3ff097f91aff33d0d42e75df25f676cd2581"

