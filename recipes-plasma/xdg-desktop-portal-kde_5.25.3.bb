# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/xdg-desktop-portal-kde-5.25.3.tar.xz"
SRC_URI[sha256sum] = "c79ebabcd7a5c418de396a6e4e454cac91037db4021bdd381766057970bfec1e"

