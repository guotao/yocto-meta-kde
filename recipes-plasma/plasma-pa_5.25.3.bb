# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-pa-5.25.3.tar.xz"
SRC_URI[sha256sum] = "41603522d62cb329a4a77c3219317565d434a150a4bf6ce05bb538e7c00e089d"

