# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/layer-shell-qt-5.25.3.tar.xz"
SRC_URI[sha256sum] = "60c5172a66562f1a12b8d27bc632e80eb721fb7dab89c5f3d7ac12c849c53d1b"

