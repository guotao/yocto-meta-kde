# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/kdecoration-5.25.3.tar.xz"
SRC_URI[sha256sum] = "3ef59443be9b602a85710d0de1d39d9e9f5acc98698c0d13628ad66627b6de2c"

