# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/discover-5.25.3.tar.xz"
SRC_URI[sha256sum] = "3a53d8501f33c8a035101f9d33546efa8633a94dbaef4b88afd57f42fb2baf2d"

