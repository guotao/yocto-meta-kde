# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/breeze-5.25.3.tar.xz"
SRC_URI[sha256sum] = "1c5fdf94cf7cfba0a396b6945639eee28d230213d4bf8e05ccbd31f7262b477d"

