# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/libkscreen-5.25.3.tar.xz"
SRC_URI[sha256sum] = "e59fd83678eaa726d72afda7313d081943cb63b9abfbe85627dc334ec0ccb3d7"

