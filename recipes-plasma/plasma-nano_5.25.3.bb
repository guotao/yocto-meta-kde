# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-nano-5.25.3.tar.xz"
SRC_URI[sha256sum] = "6d0e3640d7fdd6020eb3c84afa0e87255709a55d76930638dae88dc5022ba902"

