# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/milou-5.25.3.tar.xz"
SRC_URI[sha256sum] = "57915f252b8b14c8387a2a518fe7e0fa1344be848c5214197d9564635b8963f5"

