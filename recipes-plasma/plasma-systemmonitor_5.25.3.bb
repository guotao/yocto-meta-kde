# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/plasma-systemmonitor-5.25.3.tar.xz"
SRC_URI[sha256sum] = "1d9ac656721ae82795f6299790214ae1f4c0089338921b188e1d60eb419dbd1e"

