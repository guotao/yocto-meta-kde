# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/polkit-kde-agent-1-5.25.3.tar.xz"
SRC_URI[sha256sum] = "95dd7433f42a6afbd01e03783fb36d4fc55ec7d8d7b1ab4146637e83f43d0648"

