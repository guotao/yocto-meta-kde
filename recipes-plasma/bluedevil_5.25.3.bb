# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/bluedevil-5.25.3.tar.xz"
SRC_URI[sha256sum] = "656173f9d18ce153dc5b3638006d0eefa809e57c4c66aad1a118d1d272a93615"

