# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.25.3/systemsettings-5.25.3.tar.xz"
SRC_URI[sha256sum] = "99d0ec23f63934c93ce3de99e61bd21f9552b615eaebdcb4e36c808b43c9c402"

