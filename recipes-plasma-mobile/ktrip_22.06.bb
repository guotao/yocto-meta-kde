# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/ktrip-22.06.tar.xz"
SRC_URI[sha256sum] = "145f6a59b383feb527bb7369a86552b33a6bdbd1eb19f5de859d34a430386788"

