# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/plasma-settings-22.06.tar.xz"
SRC_URI[sha256sum] = "550834aef84cd53ca56d764e647d47f9a0ead29dbe556b39226901e7faf7df24"

