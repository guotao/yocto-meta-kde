# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/koko-22.06.tar.xz"
SRC_URI[sha256sum] = "b452def725d955e7d9f5a8520bb54a0a136504eea6230b9f58b501dc8fa69c39"

