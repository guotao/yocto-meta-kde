# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/kongress-22.06.tar.xz"
SRC_URI[sha256sum] = "2b6bdc62f1f0b4bd4f750a63c037cb1d74c4a02127295318f922040f76c39ed6"

