# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/kasts-22.06.tar.xz"
SRC_URI[sha256sum] = "3353997cecaaeb8129b810973e751d221b852bc8fcd0e9970e1fe6fe4855f643"

