# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/krecorder-22.06.tar.xz"
SRC_URI[sha256sum] = "6f907056f58dcbe4292c2b434420a2fb560eb46d02695801ef3728dfcbc70f91"

