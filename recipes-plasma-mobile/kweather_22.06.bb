# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/kweather-22.06.tar.xz"
SRC_URI[sha256sum] = "f317d9a978e4cfab350fc0eda7bbe90315033eb6eec13d6c73e19bbb3fd78728"

