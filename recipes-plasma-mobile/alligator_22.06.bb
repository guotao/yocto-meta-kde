# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/alligator-22.06.tar.xz"
SRC_URI[sha256sum] = "ae76960d29e247c7150114a097b860b483dddebcc230a9624076c6583f5792c0"

