# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/calindori-22.06.tar.xz"
SRC_URI[sha256sum] = "073c396948a1565d2c5007a9da2ebed7f152a2e989a98dc337ade9e8c39c8b39"

