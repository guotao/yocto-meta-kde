# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/22.06/kclock-22.06.tar.xz"
SRC_URI[sha256sum] = "32ef3245b6387418abc6a63750c142b375b14bc63b15cb650fb0dcc1567fa2e5"

